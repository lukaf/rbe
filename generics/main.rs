#[derive(Debug)]
struct A;

#[derive(Debug)]
struct Single(A);

#[derive(Debug)]
struct SingleGen<T>(T);

fn main() {
    let s = Single(A);
    println!("Single(A): {:?}", s);

    let c: SingleGen<char> = SingleGen('a');
    // type can also be guessed by the compiler
    // let c = SingleGen('a');
    println!("SingleGen('a'): {:?}", c);

    println!("SingleGen(6): {:?}", SingleGen(6));
}
