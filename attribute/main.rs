#[cfg(condition = "kekec")]
fn function() {
    println!("kekec");
}

#[cfg(condition = "pehta")]
fn function() {
    println!("pehta");
}

fn main() {
    function();
}
