#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    // static method don't neet to be called by an instance
    fn origin() -> Self {
        Self { x: 0, y: 0 }
    }

    // another static method
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    // instance method
    // same as: fn print(self: &Self)
    fn print(self: &Self) {
        println!("Point {{ x: {}, y: {} }}", self.x, self.y)
    }
}

// same as: fn main() -> () {
// functions without return values return the unit type ()
fn main() {
    let point = Point::new(1, 2);
    let origin = Point::origin();

    println!("Point: {:?}", point.print());
    println!("Origin: {:?}", origin.print());
}
