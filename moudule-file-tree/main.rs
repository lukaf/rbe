mod fromfile;

mod fromdir;

fn main() {
    fromfile::function();
    fromdir::function();
}
