#![allow(overflowing_literals)]

fn main() {
    let name = "Luka";
    for c in name.chars() {
        println!("{} => {}", c, c as u8);
    }

    // if value doesn't fit into te type it gets truncated
    // see the overflowing_literals attribute at the top of
    // the file
    println!("1000 as u8: {}", 1000 as u8);
    println!("10000 mod 256: {}", 1000 % 256);

    let decimal = 1.2;
    let integer = decimal as u8;
    println!("decimal: {}, integer: {}", decimal, integer);
}
