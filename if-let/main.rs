enum Foo {
    Bar,
    Qux(i32),
}

fn main() {
    let x = Some(7);
    if let Some(value) = x {
        println!("x is Some and has value: {}", value);
    }

    let bar = Foo::Bar;
    let qux = Foo::Qux(9);

    if let Foo::Bar = bar {
        println!("bar is Foo::Bar");
    }

    if let Foo::Qux(value) = qux {
        println!("qux is Foo::Qux with value: {}", value);
    }

    let mut something = Some(7);
    while let Some(i) = something {
        if i > 9 {
            println!("Greater than 9, quit");
            something = None;
        } else {
            println!("Incrementing");
            something = Some(i + 1);
        }
    }
}
