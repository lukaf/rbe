#![allow(dead_code)]

enum WebEvent {
    PageLoad,
    PageUnload,
    KeyPress(char),
    Paste(String),
    Click { x: i64, y: i64 },
}

fn inspect(event: WebEvent) {
    match event {
        WebEvent::PageLoad => println!("Page loaded"),
        WebEvent::PageUnload => println!("Page unloaded"),

        // behold, the descructuring
        WebEvent::KeyPress(c) => println!("Pressed: '{}'", c),
        WebEvent::Paste(s) => println!("Pasted \"{}\"", s),
        WebEvent::Click { x, y } => println!("Clicked at x={}, y= {}", x, y),
    }
}

enum ImplicitDiscriminator {
    Zero,
    One,
    Two,
}

enum ExplicitDiscriminator {
    Red = 0xff0000,
    Green = 0x00ff00,
    Blue = 0x0000ff,
}

fn main() {
    let pressed = WebEvent::KeyPress('x');
    let pasted = WebEvent::Paste("something".to_owned());
    let click = WebEvent::Click { x: 20, y: 80 };
    let load = WebEvent::PageLoad;
    let unload = WebEvent::PageUnload;

    inspect(pressed);
    inspect(pasted);
    inspect(click);
    inspect(load);
    inspect(unload);

    println!(
        "Implicitdiscriminator Zero: {}",
        ImplicitDiscriminator::Zero as i32
    );
    println!(
        "Implicitdiscriminator Two: {}",
        ImplicitDiscriminator::Two as i32
    );

    println!(
        "ExplicitDiscriminator Red: {}",
        ExplicitDiscriminator::Red as i32
    );
}
