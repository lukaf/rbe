use std::convert::{From, Into};
use std::convert::{TryFrom, TryInto};

#[derive(Debug)]
struct Number {
    value: i32,
}

impl From<i32> for Number {
    fn from(item: i32) -> Self {
        Number { value: item }
    }
}

#[derive(Debug)]
struct EvenNumber(i32);

impl TryFrom<i32> for EvenNumber {
    type Error = ();

    fn try_from(item: i32) -> Result<Self, Self::Error> {
        if item % 2 == 0 {
            Ok(Self(item))
        } else {
            Err(())
        }
    }
}

fn main() {
    let i: i32 = 10;
    println!("i: {:?}", i);

    {
        let number = Number::from(i);
        println!("number (From): {:?}", number);
    }

    {
        let number: Number = i.into();
        println!("number (Into): {:?}", number);
    }

    {
        println!("EvenNumber::try_from(8): {:?}", EvenNumber::try_from(8));
        println!("EvenNumber::try_from(5): {:?}", EvenNumber::try_from(5));

        let result: Result<EvenNumber, ()> = 8i32.try_into();
        println!("8i32.try_into(): {:?}", result);

        let result: Result<EvenNumber, ()> = 5i32.try_into();
        println!("5i32.try_into(): {:?}", result);
    }
}
