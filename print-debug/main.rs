#[derive(Debug)]
struct Structure(i32);

#[derive(Debug)]
struct Deep(Structure);

fn main() {
    println!("Structure: {:?}", Structure(3));
    println!("Deep structure: {:?}", Deep(Structure(3)));

    // all std library types automatically implement Debug
    println!("This is number 12: {:?}", 12);

    println!(
        "{1:?} {0:?} is the {2:?} name",
        "Slater", "Christian", "author's"
    );

    // pretty printing with {:#?}
    println!("Structure: {:#?}", Structure(3));
    println!("Deep structure: {:#?}", Deep(Structure(7)));

    #[derive(Debug)]
    struct Person<'a> {
        name: &'a str,
        age: u8,
    };

    let name = "Luka";
    let age = 38;
    let person = Person { name, age };

    println!("{:#?}", person);
}
