fn function() {
    println!("Called function()");
}

mod cool {
    pub fn function() {
        println!("Called public cool::function()");
    }
}

mod my {
    fn function() {
        println!("Called private my::function()");
    }

    mod cool {
        pub fn function() {
            println!("Called public my::cool::function()");
        }
    }

    pub fn indirect_call() {
        println!("Called public my::indirect_call()");

        // the following are the same
        println!("my::indirect_call calling self::function()");
        self::function();

        println!("my::indirect_call calling function()");
        function();

        println!("my::indirect_call calling self::cool::function()");
        self::cool::function();

        println!("my::indirect_call calling super::function()");
        super::function();

        {
            println!(
                "scope in my::indirect_call() calling crate::cool::function as root_function()"
            );
            use crate::cool::function as root_function;
            root_function();

            println!("scope in my::indirect_call() calling crate::function as f()");
            use crate::function as f;
            f();
        }
    }
}

fn main() {
    my::indirect_call();
}
