fn main() {
    fn function(i: i32) -> i32 {
        i + 1
    }

    // same with closure
    let closure_annotated = |i: i32| -> i32 { i + 1 }; // annotating the result requires {}
    let closure_inferred = |i| i + 1;

    let i = 10;

    println!("Function: {}", function(i));
    println!("Closure annotated: {}", closure_annotated(i));
    println!("Closure inferred: {}", closure_inferred(i));

    // strange case or very limited closure which only returns 1:
    let one = || 1;
    println!("one: {}", one());

    // closure and function as a parameter
    {
        println!("Closure(s) and function(s) as parameter(s)");

        fn call_me<F: Fn()>(f: F) {
            f();
        }

        fn call_me_with_arg<F: Fn(i32)>(f: F) {
            f(3);
        }

        let closure = || println!("I am a closure");
        let closure_with_arg = |x: i32| println!("I am a closure, my x: {}", x);

        fn function() {
            println!("I am a function");
        }
        fn function_with_arg(x: i32) {
            println!("I am a function, my x: {}", x);
        }

        call_me(closure);
        call_me(function);

        call_me_with_arg(closure_with_arg);
        call_me_with_arg(function_with_arg);
    }
}
