#[derive(Debug)]
struct One {
    number: Option<i32>,
}

fn main() {
    let x = Some(One { number: None });
    let y = Some(One { number: Some(10) });

    println!("x: {:?}", x.map(|a| a.number).map(|b| b));
    println!("y: {:?}", y.map(|a| a.number).map(|b| b));
}
