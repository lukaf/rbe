fn main() {
    let x = Some("x");
    let y = Some("y");
    let z = None;

    println!("match_option:");
    match_option(x);
    match_option(y);
    match_option(z);

    println!("unwrap_option:");
    unwrap_option(x);
    unwrap_option(y);
    // this panics due to unwrapping None
    // unwrap_option(z);

    println!("iflet_option:");
    iflet_option(x);
    iflet_option(y);
    iflet_option(z);
}

fn match_option(x: Option<&str>) {
    match x {
        Some("x") => println!("x"),
        Some(value) => println!("something: {}", value),
        None => println!("none"),
    }
}

fn unwrap_option(x: Option<&str>) {
    let inside = x.unwrap();
    println!("inside: {}", inside);
}

fn iflet_option(x: Option<&str>) {
    if let Some(value) = x {
        println!("something: {}", value);
    } else {
        println!("none");
    }
}
