fn main() {
    let v = vec!["tofu", "1", "100", "123"];
    collect_all(v.clone());
    ignoring_failed_attempts(v.clone());
    fail_all_with_collect(v.clone());
    split_values_and_errors(v);
}

fn collect_all(v: Vec<&str>) {
    let numbers: Vec<_> = v.into_iter().map(|s| s.parse::<i32>()).collect();
    println!("collect_ll numbers: {:?}", numbers);
}

fn ignoring_failed_attempts(v: Vec<&str>) {
    let numbers: Vec<_> = v
        .into_iter()
        .map(|s| s.parse::<i32>())
        .filter_map(Result::ok) // if closure returns Some(item), item is returned
        .collect();
    println!("ignoring_failed_attempts numbers: {:?}", numbers);
}

fn fail_all_with_collect(v: Vec<&str>) {
    let numbers: Result<Vec<_>, _> = v.into_iter().map(|s| s.parse::<i32>()).collect();
    println!("fail_all_with_collect numbers: {:?}", numbers);
}

fn split_values_and_errors(v: Vec<&str>) {
    let (numbers, errors): (Vec<_>, Vec<_>) = v
        .into_iter()
        .map(|s| s.parse::<i32>())
        .partition(Result::is_ok);

    // these two lines just unwrap results from Result
    let numbers: Vec<_> = numbers.into_iter().map(Result::unwrap).collect();
    let errors: Vec<_> = errors.into_iter().map(Result::unwrap_err).collect();

    println!("split_values_and_errors numbers: {:?}", numbers);
    println!("split_values_and_errors errors: {:?}", errors);
}
