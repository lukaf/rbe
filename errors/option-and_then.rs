fn main() {
    {
        println!("With Some(10)");
        let x = Some(10);

        // prints 10
        x.and_then(|x: i32| {
            println!("{:?}", x);
            Some(x)
        });
    }

    {
        println!("With None");
        let x = None;

        x.and_then(|x: i32| {
            println!("{:?}", x);
            Some(x)
        });
    }
}
