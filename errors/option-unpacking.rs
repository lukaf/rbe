#[derive(Debug)]
struct Person {
    job: Option<Job>,
}

#[derive(Debug, Clone, Copy)]
struct Job {
    salary: Option<Salary>,
}

#[derive(Debug, Clone, Copy)]
struct Salary {
    sum: Option<u32>,
}

impl Person {
    fn salary(&self) -> Option<u32> {
        self.job?.salary?.sum
    }
}

fn main() {
    let person = Person {
        job: Some(Job {
            salary: Some(Salary { sum: Some(100) }),
        }),
    };

    println!("{:?}", person.salary());
}
