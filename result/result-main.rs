use std::num::ParseIntError;

fn main() -> Result<(), ParseIntError> {
    let x = "x";

    let number = match x.parse::<i32>() {
        Ok(number) => number,
        Err(e) => return Err(e),
    };

    println!("{}", number);
    Ok(())
}
