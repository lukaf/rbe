use std::fmt;

#[derive(Debug)]
struct Point {
    x: f32,
    y: f32,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Point({}, {})", self.x, self.y)
    }
}

#[allow(dead_code)]
struct Rectangle {
    top_left: Point,
    bottom_right: Point,
}

#[allow(dead_code)]
fn rect_area(rectangle: &Rectangle) -> f32 {
    let Rectangle {
        top_left: Point { x: x1, y: y1 },
        bottom_right: Point { x: x2, y: y2 },
    } = rectangle;

    let width = x2 - x1;
    let height = y1 - y2;

    width * height
}

#[allow(dead_code)]
fn square(point: Point, wh: f32) -> Rectangle {
    Rectangle {
        top_left: Point {
            y: point.y + wh,
            ..point
        },
        bottom_right: Point {
            x: point.x + wh,
            ..point
        },
    }
}

fn main() {
    let point = Point { x: 1.2, y: 10.3 };
    println!("Point: {}", point);

    // struct update syntax
    let new_x = 3.0;
    let point_with_new_x = Point { x: new_x, ..point };
    println!("New x: {}", new_x);
    println!("Point with new x: {}", point_with_new_x);

    let new_y = 11.2;
    let point_with_new_y = Point { y: new_y, ..point };
    println!("New y: {}", new_y);
    println!("Point with new y: {}", point_with_new_y);

    // destructure Point
    let Point { x: d_x, y: d_y } = point_with_new_y;
    println!("Destructured coordinates: {} {}", d_x, d_y);
}
