fn combine_vec(x: Vec<i32>, y: Vec<i32>) -> impl Iterator<Item = i32> {
    x.into_iter().chain(y.into_iter()).cycle()
}

fn main() {
    let x = vec![1, 2, 3, 4];
    let y = vec![10, 20, 30, 40];

    for i in combine_vec(x, y).take(10) {
        println!("{}", i);
    }
}
