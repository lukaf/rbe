fn function(x: i32) -> impl Fn(i32) -> i32 {
    let closure = move |y: i32| x + y;
    closure
}

fn main() {
    let f = function(10);
    println!("{}", f(3));
}
