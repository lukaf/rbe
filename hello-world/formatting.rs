fn main() {
    // by default, 31 is 31i32
    println!("{} days", 31);
    println!("{} days", 31i32);

    // positional arguments
    println!("{0} this is {1}, {1} this is {0}", "Alice", "Bob");

    // named arguments
    println!(
        "{subject} {verb} {object}",
        object = "the lazy dog",
        subject = "the quick brown fox",
        verb = "jumps over"
    );

    // special formatting with :
    println!(
        "{} of {:b} people know binary, the other half doesn't",
        1, 2
    );

    // align
    println!("{number:>width$}", number = 1, width = 6);

    // aligning with non-white space (padding)
    println!("{number:0>width$}", number = 1, width = 6);

    // it checks for the correct number of arguments
    // println!("I miss an argument {0} {1}", "one");
    // println!("I have an extra argument {0}", "one", "two");

    let x = format!("This is a formatted {}", "string");
    println!("{}", x);

    eprintln!("This goes to stderr");

    #[allow(dead_code)]
    struct Structure(i32);
    // println!("Structure {}", Structure(3));

    let pi = 3.141592;
    println!("Pi is roughly {:.3}", pi);
}
