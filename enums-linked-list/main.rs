#![allow(dead_code)]

enum List {
    Cons(u32, Box<List>),
    Nil,
}

impl List {
    fn new() -> List {
        List::Nil
    }

    fn prepend(self, elem: u32) -> List {
        List::Cons(elem, Box::new(self))
    }

    fn len(&self) -> u32 {
        match *self {
            Self::Nil => 0,
            Self::Cons(_, ref tail) => 1 + tail.len(),
        }
    }

    fn stringify(&self) -> String {
        match *self {
            Self::Cons(head, ref tail) => format!("{}, {}", head, tail.stringify()),
            Self::Nil => format!("Nil"),
        }
    }
}

fn main() {
    // Create an empty linked list
    let mut list = List::new();

    // Prepend some elements
    list = list.prepend(1);
    list = list.prepend(2);
    list = list.prepend(3);

    // Show the final state of the list
    println!("linked list has length: {}", list.len());
    println!("{}", list.stringify());
}
