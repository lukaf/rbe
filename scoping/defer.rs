struct File;

impl Drop for File {
    fn drop(&mut self) {
        println!("This is where we close the file");
    }
}

fn main() {
    println!("Open file");
    let _f = File;
}
