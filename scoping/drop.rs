struct DropThis;

impl Drop for DropThis {
    fn drop(&mut self) {
        println!("DropThis is being dropped");
    }
}

fn main() {
    let _x = DropThis;
    println!("Made a DropThis");
}
