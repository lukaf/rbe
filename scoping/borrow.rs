fn main() {
    let mut _x = 10;

    {
        let y = &_x;

        // this fails because borrowed value is used in the println statement
        // after this assingment
        // _x = 20;

        println!("y = {}", y);

        // this works because there are no more uses of the borrwed value
        _x = 20;
    }

    _x = 20;
}
